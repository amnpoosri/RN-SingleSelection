import React, {Component} from 'react';
import {Platform, View, StyleSheet, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

import {moderateScale} from '../../utils/scaling';

class Header extends Component {

  static propTypes = {
    onClosePress: PropTypes.func,
    onConfirmPress: PropTypes.func,
    isCloseEnable: PropTypes.bool,
    isConfirmEnable: PropTypes.bool,
  };

  static defaultProps = {
    onClosePress: () => {},
    onConfirmPress: () => {},
    isCloseEnable: true,
    isConfirmEnable: true
  };
  render() {
    return (
        <View style={styles.headerContainer}>
            {this.props.isCloseEnable && <TouchableOpacity 
              style={styles.closeContainer}
              onPress={this.props.onClosePress}>
              <MaterialCommunityIcon
                name="close-box"
                size={48}
                color={'red'}
                style={styles.closeIcon}/>
            </TouchableOpacity>}
            {this.props.isConfirmEnable && <TouchableOpacity 
              style={styles.confirmContainer}
              onPress={this.props.onConfirmPress}>
              <MaterialCommunityIcon
                name="checkbox-marked"
                size={48}
                color={'green'}
                style={styles.confirmIcon}/>
            </TouchableOpacity>}
        </View>
    );
  }
}

const styles = StyleSheet.create({
  headerContainer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginTop: Platform.OS === 'ios' ? 20 : 0,
  },
  closeContainer: {
    // paddingVertical: 2,
    // paddingHorizontal: 8,
    // borderRadius: 4
  },
  confirmContainer: {
    // backgroundColor: 'green',
    // paddingVertical: 2,
    // paddingHorizontal: 8,
    // borderRadius: 4
  },
  closeIcon: {  

  },
  confirmIcon: {
    alignItems: 'center',
    justifyContent: 'center'
  }

});

export default Header;