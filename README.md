# React-Native Single-Selection Component
A single selecttion component is made with react-native-modal. The selected item will move to the top and ordey and arrange unselected item in alphabetical order. Search component is used to filter the search item.

IOS            |  ANDROID
:-------------------------:|:-------------------------:
<img src="preview/ios_single_selection.gif" alt="IOS" width="250">  |  <img src="preview/android_single_selection.gif" alt="IOS" width="250">
        

## Usage

Required props:  
`items` | array  
`uniqueKey` | string  
`onSelectedItemsChange` | function  

```javascript
import React, {Component} from 'react';
import { StyleSheet, View, StatusBar, TouchableHighlight, Text} from 'react-native';
import Modal from 'react-native-modal'
import {isEmpty} from 'lodash';

import SingleSelection from './components/SingleSelection'
const items = [
      {
          "id": 11,
          "name": "Airsoft"
      },
      {
          "id": 21,
          "name": "Basketball"
      },
      {
          "id": 31,
          "name": "biking"
      },
      {
          "id": 41,
          "name": "Bootcamp"
      },
      {
          "id": 51,
          "name": "Boxing"
      },
      {
          "id": 61,
          "name": "Energetic meditation"
      },
      {
          "id": 71,
          "name": "Energy meditation"
      },
      {
          "id": 81,
          "name": "Enery Meditation"
      },
      {
          "id": 91,
          "name": "Football"
      },
      {
          "id": 101,
          "name": "Kickboxing"
      },
      {
          "id": 111,
          "name": "Running"
      },
      {
          "id": 121,
          "name": "Skateboarding"
      },
      {
          "id": 131,
          "name": "Street soccer"
      },
      {
          "id": 141,
          "name": "Street workout"
      },
      {
          "id": 151,
          "name": "swimming"
      },
      {
          "id": 161,
          "name": "Volleyball"
      },
      {
          "id": 171,
          "name": "Yoga"
      }]
  

export default class App extends Component {

    static propTypes = {

      };

    constructor(props: PropsType) {
        super(props);
        this.state = {
            isSingleSelectionVisible: false,
            selectedItem: {}
          }  
      }

    onItemSelected = selectedItem => {
        this.setState({ selectedItem });
        this.hideSingleSelectionModal()
      };

    showSingleSelectionModal = () => {
        this.setState({ isSingleSelectionVisible: true });
      };

    hideSingleSelectionModal = () => {
        this.setState({ isSingleSelectionVisible: false });
      };  


  render() {
    return (
      <View style={styles.container}>
        {/* Item */}
        <View style={styles.itemContainer}>
            <TouchableHighlight 
                onPress={this.showSingleSelectionModal} 
                underlayColor='white'
                style={styles.button}>
              <View style={styles.itemInlineButton}>
                <Text style={styles.itemText}>
                  {isEmpty(this.state.selectedItem) ? 'Choose a sport': this.state.selectedItem.name}
                </Text>
              </View>
            </TouchableHighlight>
        </View>
        
        {/* Single Selection Modal */}
       <Modal
        isVisible={this.state.isSingleSelectionVisible}
        style={styles.singleSelectionModal}
        onBackButtonPress={this.hideSingleSelectionModal}>
        <SingleSelection 
          items={items}
          uniqueKey="id"
          displayKey="name"
          selectedItem={this.state.selectedItem}
          onItemSelected={this.onItemSelected}/>
        </Modal>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  button: {
    flex: 1,
    alignSelf: 'stretch',
  },
  itemContainer: {
    borderColor: '#ddd',
    backgroundColor: '#f8f8f8',
    borderWidth: 1,
    margin: -1,
    height: 47,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemInlineButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column'
  },
  itemText: {
    color: '#79797a',
    fontSize: 14,
    fontWeight: 'bold',
  },
  singleSelectionModal: {
    backgroundColor: 'white',
    margin: 0, // This is the important style you need to set
    alignItems: undefined,
    justifyContent: undefined,
  }

});
```

### Props
| Prop        					| Default       | type 			| Desc  |
| ------------- 				|-------------	| -----			|-----	|
|uniqueKey              | 'id'          | string    |the unique key for your items |
|displayKey           | 'name'         | string    |the key for the display name / title of the item |
|selectedItem					| {}						| object 		|the selected items |
|onItemSelected	| 	| function	|function that runs when an item is selected|
|onClosePress	| 	| function	|function that closes Single Option Modal|